from persiantools.jdatetime import JalaliDateTime, JalaliDate
from persiantools import digits
import datetime


def get_jalali_date(in_date):
    in_date_j = JalaliDate(in_date)
    
    dic_day = {
        'Shanbeh': 'شنبه',
        'Yekshanbeh': 'یکشنبه',
        'Doshanbeh': 'دوشنبه',
        'Seshanbeh': 'سه شنبه',
        'Chaharshanbeh': 'چهارشنبه',
        'Panjshanbeh': 'پنج شنبه',
        'Jomeh': 'جمعه'
    }

    dic_month = {
    'Farvardin': 'فروردین',
    'Ordibehesht': 'اردیبهشت',
    'Khordad': 'خرداد',
    'Tir': 'تیر',
    'Mordad': 'مرداد',
    'Shahrivar': 'شهریور',
    'Mehr': 'مهر',
    'Aban': 'آبان',
    'Azar': 'آذر',
    'Dey': 'دی',
    'Bahman': 'بهمن',
    'Esfand': 'اسفند'
    }
    farsi_day_of_week = dic_day[in_date_j.strftime("%A")]
    farsi_month = dic_month[in_date_j.strftime("%B")]
    farsi_year = digits.en_to_fa(str(in_date_j.strftime("%Y")))
    farsi_day = digits.en_to_fa(str(in_date_j.strftime("%d")))
    complete_date = farsi_day_of_week + ' ' + farsi_day + ' ' + farsi_month + ' ' + farsi_year
    
    return complete_date



def test():
    ntime = datetime.datetime.now()
    with open('myfile.txt', encoding='utf-8', mode='w') as f:
        for i in range(24):
            ntime = ntime + datetime.timedelta(weeks=2)
            f.write(get_jalali_date(ntime)+'\n')



