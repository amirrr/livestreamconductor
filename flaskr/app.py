
# A very simple Flask Hello World app for you to get started with...

from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello from Flask!'


@app.route('/login')
def login():
    return render_template('user/login.html')



@app.route('/profile')
def profile():
    return render_template('user/profile.html')

