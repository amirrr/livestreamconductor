## Database model for time frames


### class livestream
- datetime time_from
- datetime time_to
- varchar allowd_audince_username   // if its private live stream
- varchar name
- datetime duration                 // caluclated automaticaly upon class creation
- foreign_key programs(one_to_many)

### class programs
- datetime time_from
- datetime time_to
- varchar name
- varchar description
- varint position_in_que
- datetime duration


## function description

- check_overlap : checks for overlap of the entred times of programs or streams.
- view_create : view for creating the conductor of the livestream
- socket_func : in general socket functionalites to get the livestream and send JSON
- get_stream : that gets the schedule for the stream and displays them for the user

